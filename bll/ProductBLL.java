package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.*;
import model.*;

public class ProductBLL{

    public Product findById(int id) {
        ProductDAO d = new ProductDAO();
        Product st =d.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The order with id =" + id + " was not found!");
        }
        return st;
    }

    public void insertProduct(Product product) throws IllegalArgumentException, IllegalAccessException {
        ProductDAO d = new ProductDAO();
        d.insert(product);
    }

    public void deleteProduct(int id) {
        ProductDAO d = new ProductDAO();
        d.delete(id);
    }

    public void updateProduct(Product product, int id) {
        ProductDAO d = new ProductDAO();
        d.update(id, product);
    }

    public List<Product> findAllProduct(){
        ProductDAO d = new ProductDAO();
        List<Product> st=d.findAll();
        return st;
    }

}
