package bll;

import java.util.List;
import java.util.NoSuchElementException;

import dao.*;
import model.*;

public class OrderBLL{


    public Order findOrderById(int id) {
        OrderDAO d = new OrderDAO();
        Order st =d.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The order with id =" + id + " was not found!");
        }
        return st;
    }

    public void insertOrder(Order order) throws IllegalArgumentException, IllegalAccessException {
        OrderDAO d = new OrderDAO();
        d.insert(order);

    }

    public void deleteOrder(int id) {
        OrderDAO d = new OrderDAO();
        d.delete(id);
    }

    public void updateOrder(Order order, int id) {
        OrderDAO d = new OrderDAO();
        d.update(id, order);
    }

    public List<Order> findAllOrder(){
        OrderDAO d = new OrderDAO();
        List<Order> st=d.findAll();
        return st;
    }

    public List<Order> findAllOrdersPerId(int client) {
        OrderDAO d = new OrderDAO();
        List<Order> st=d.findAllOrdersPerId(client);
        return st;
    }
}
