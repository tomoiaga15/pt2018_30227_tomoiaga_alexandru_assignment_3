package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import validators.*;
import dao.*;
import model.*;

public class CostumerBLL{

    private List<Validator<Costumer>> validators;

    public CostumerBLL() {
        validators = new ArrayList<Validator<Costumer>>();
        validators.add(new CNPValidator());
        validators.add(new TelefonValidator());
    }


    public Costumer findCostumerById(int id) {
        CostumerDAO d = new CostumerDAO();
        Costumer st =d.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The costumer with id =" + id + " was not found!");
        }
        return st;
    }
    public void insertCostumer(Costumer costumer) throws IllegalArgumentException, IllegalAccessException {
        for (Validator<Costumer> v : validators) {
            v.validate(costumer);
        }
        CostumerDAO d = new CostumerDAO();
        d.insert(costumer);
    }

    public void deleteCostumer(int id) {
        CostumerDAO d = new CostumerDAO();
        d.delete(id);

    }

    public void updateCostumer(Costumer costumer, int id) {
        CostumerDAO d = new CostumerDAO();
        d.update(id, costumer);
    }

    public List<Costumer> findAllCostumer(){
        CostumerDAO d = new CostumerDAO();
        List<Costumer> st=d.findAll();
        return st;
    }
}