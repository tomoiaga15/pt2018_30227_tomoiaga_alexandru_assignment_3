package dao;

import com.mysql.jdbc.PreparedStatement;
import connection.ConnectionFactory;
import model.Order;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

public class OrderDAO extends DAO<Order>{

    public List<Order> findAllOrdersPerId(int id) {
        Connection dbConnection = null;
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        String query = "SELECT * FROM order WHERE idClient = ?";
        try {
            dbConnection = ConnectionFactory.getConnection();
            findStatement = (PreparedStatement) dbConnection.prepareStatement(query);
            findStatement.setInt(1, id);
            rs = findStatement.executeQuery();
            return createObjects(rs);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:findAllOrdersPerId " + e.getMessage());
        } finally {

            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return null;
    }

    public void deleteAllOrderPerId(int id){
        Connection dbConnection = null;
        PreparedStatement findStatement = null;
        String query = "DELETE FROM order WHERE idClient = ?";
        try {
            dbConnection = ConnectionFactory.getConnection();
            findStatement = (PreparedStatement) dbConnection.prepareStatement(query);
            findStatement.setInt(1, id);
          findStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:findAllOrdersPerId " + e.getMessage());
        } finally {

            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
    }
}
