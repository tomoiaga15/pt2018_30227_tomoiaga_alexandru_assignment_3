package dao;

import java.beans.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import java.util.logging.*;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import connection.ConnectionFactory;


public class DAO<T> {

    protected static final Logger LOGGER = Logger.getLogger(DAO.class.getName());

    private final Class<T> type;

    @SuppressWarnings("unchecked")

    public DAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }

    private String createInsertQuery(T t) throws IllegalArgumentException, IllegalAccessException {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(type.getSimpleName() + " (");
        for (Field field : t.getClass().getDeclaredFields()) {
            sb.append(field.getName() + ",");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(")");
        sb.append("VALUES ( ?, ?, ?, ?)");
        return sb.toString();
    }

    private String createUpdateQuery(T t, String name) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(type.getSimpleName() + " SET ");
        for (Field field : t.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if(!field.getName().equals("id"))
            sb.append(field.getName() + "= ?,");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(" WHERE " + name + " = ?");
        return sb.toString();
    }

    private String createDeleteQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }

    private String createFindAllQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM ");
        sb.append(type.getSimpleName());
        return sb.toString();
    }

    public T findById(int id) {

        Connection dbConnection = null;
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        String query = createSelectQuery("id");
        try {
            dbConnection = ConnectionFactory.getConnection();
            findStatement = (PreparedStatement) dbConnection.prepareStatement(query);
            findStatement.setInt(1, id);
            rs = findStatement.executeQuery();
            return createObjects(rs).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return null;
    }
    public List<T> createObjects(ResultSet rs) {
        List<T> list = new ArrayList<T>();

        try {
            while (rs.next()) {
                T instance = type.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    field.setAccessible(true);
                    Object value = rs.getObject(field.getName());
                    PropertyDescriptor pd = new PropertyDescriptor(field.getName(), type);
                    Method method = pd.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }
    public void insert(T t) throws IllegalArgumentException, IllegalAccessException {
        Connection dbConnection = null;
        PreparedStatement insertStatement = null;
        String insertStatementString = createInsertQuery(t);
        try {
            dbConnection = ConnectionFactory.getConnection();
            int poz = 1;
            insertStatement = (PreparedStatement) dbConnection.prepareStatement(insertStatementString,
                    Statement.RETURN_GENERATED_KEYS);
            for (Field field : t.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if (field.get(t) instanceof Integer)
                    insertStatement.setInt(poz, (Integer) field.get(t));
                else
                    insertStatement.setString(poz, (String) field.get(t));
                poz++;

            }
            insertStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
    }
    public void update(int id, T t) {
        Connection dbConnection = null;
        PreparedStatement findStatement = null;
        String query = createUpdateQuery(t, "id");
        try {
            dbConnection = ConnectionFactory.getConnection();
            findStatement = (PreparedStatement) dbConnection.prepareStatement(query);
            int poz=1;
            for (Field field : t.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if(!field.getName().equals("id")){
                if (field.get(t) instanceof Integer)
                    findStatement.setInt(poz, (Integer) field.get(t));
                else
                    findStatement.setString(poz, (String) field.get(t));
                poz++;}
            }
            findStatement.setInt(poz, id);
            findStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
    }
    public void delete(int id) {
        Connection dbConnection = null;
        PreparedStatement findStatement = null;
        String query = createDeleteQuery("id");
        try {
            dbConnection = ConnectionFactory.getConnection();
            findStatement = (PreparedStatement) dbConnection.prepareStatement(query);
            findStatement.setInt(1, id);
            findStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:Delete " + e.getMessage());
        } finally {
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
    }

    public List<T> findAll() {
        Connection dbConnection = null;
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        String query = createFindAllQuery();
        try {
            dbConnection = ConnectionFactory.getConnection();
            findStatement = (PreparedStatement) dbConnection.prepareStatement(query);
            rs=findStatement.executeQuery();
            return createObjects(rs);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
        } finally {

            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return null;
    }
    public List<Object> change(List<T> t) {
        List<Object> list=new ArrayList();
        for(T l:t){
            list.add(l);
        }
        return list;
    }
}