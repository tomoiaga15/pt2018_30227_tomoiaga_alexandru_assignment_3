package presentation;

import dao.TableDAO;

import javax.swing.*;
import java.util.List;

class Comanda extends JFrame {

    JTable tabel;

    public Comanda(String nume,  List<Object> obj) {
        setTitle(nume);
        setSize(480, 320);
        TableDAO dao=new TableDAO();
        try {
            tabel=dao.retrieveProperties(obj);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
        p.add(tabel);
        setContentPane(p);
        setVisible(true);
    }
}