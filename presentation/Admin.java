package presentation;

import java.awt.*;
import javax.swing.*;

class Admin extends JFrame {

    JPanel panel1, panel2, panel3, panel4, panel5;
    JLabel l1, l2;
    JTextField tf1;
    JButton b1, b2, b3, b4, b5, b6, b7;

    public Admin(String nume) {
        setTitle(nume);
        setSize(480, 320);

        panel1 = new JPanel();
        panel2 = new JPanel();
        panel3 = new JPanel();
        panel4 = new JPanel();

        l1 = new JLabel("Vizualizare:");
        l2 = new JLabel("ID:");

        tf1 = new JTextField("-");

        tf1.setColumns(8);

        panel2.add(l1);
        panel4.add(l2);
        panel4.add(tf1);
        panel3.setLayout(new FlowLayout());
        panel4.setLayout(new FlowLayout());

        b1 = new JButton("Editare");
        b2 = new JButton("Clienti");
        b3 = new JButton("Produse");
        b4 = new JButton("Comenzi");
       // b5 = new JButton("Producatori");
        b6 = new JButton("Facturare");

        panel1.add(b1);
        panel3.add(b2);
        panel3.add(b3);
        panel3.add(b4);
        //panel3.add(b5);
        panel4.add(b6);

        b1.addActionListener(new Butoane(2));
        b2.addActionListener(new Butoane(3));
        b3.addActionListener(new Butoane(4));
        b4.addActionListener(new Butoane(5));
        //b5.addActionListener(new Butoane(6));
        b6.addActionListener(new Butoane(7, tf1));

        JPanel p = new JPanel();
        p.add(panel1);
        p.add(panel2);
        p.add(panel3);
        p.add(panel4);
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

        setContentPane(p);
        setVisible(true);
    }
}