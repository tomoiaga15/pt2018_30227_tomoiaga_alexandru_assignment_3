package presentation;

import bll.CostumerBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Costumer;
import model.Order;
import model.Product;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextField;

public class Butoane implements ActionListener {
    private int id, client;
    private JTextField s1, s2, s3, s4;

    public Butoane(int id, JTextField s1) {
        this.id = id;
        this.s1 = s1;
    }

    public Butoane(int id) {
        this.id = id;
    }

    public Butoane(int id, JTextField s1, JTextField s2, JTextField s3, JTextField s4) {
        this.id = id;
        this.s1 = s1;
        this.s2 = s2;
        this.s3 = s3;
        this.s4 = s4;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        CostumerBLL c = new CostumerBLL();
        ProductBLL p = new ProductBLL();
        OrderBLL o = new OrderBLL();
        List<Costumer> listC = new ArrayList();
        List<Order> listO = new ArrayList();
        List<Product> listP = new ArrayList();
        List<Product> listP1 = new ArrayList();
        List<Object> obj = new ArrayList();
        List<Object> obj1 = new ArrayList();
        String s=new String();
        switch (id) {
            case 0://open admin
                new Admin("Admin");
                break;

            case 1://open client
                client = Integer.parseInt(s1.getText());
                new Client("Client", client);
                break;
            case 2://open edit
                new Edit("Editare");
                break;
            case 3://open show clienti
                listC = c.findAllCostumer();

                for (Costumer l : listC) {
                    obj.add(l);
                }
                try {
                    new Vizualizare("Vizualizare", obj);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            case 4://open show produs
                listP1 = p.findAllProduct();
                for (Product l : listP1) {
                    obj.add(l);
                }
                try {
                    new Vizualizare("Vizualizare", obj1);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            case 5://open show order
                listO = o.findAllOrder();
                for (Order l : listO) {
                    obj.add(l);
                }
                try {
                    new Vizualizare("Vizualizare", obj);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            case 7://open prefacturare//////////////////////////////////////////////////////////////////////
                int fact=Integer.parseInt(s1.getText());

                listO = o.findAllOrdersPerId(fact);
                for (Order l : listO) {
                    s+=l.toString()+"\n";
                }
                new Factura("Factura", s);
                break;
            case 8://open show product
                listP = p.findAllProduct();
                for (Product l : listP) {
                    obj.add(l);
                }
                try {
                    new Vizualizare("Vizualizare", obj);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            case 9://open comand per id
                listO = o.findAllOrdersPerId(client);
                for (Order l : listO) {
                    obj.add(l);
                }
                new Comanda("Comanda", obj);
                break;

            case 10://insert client
                //int id = Integer.parseInt(s1.getText());
                int i = Integer.parseInt(s4.getText());
                try {
                    c.insertCostumer(new Costumer(s2.getText(), s3.getText(), i));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            case 11://insert order
                //id = Integer.parseInt(s1.getText());
                int id1, id2, id3;
                id1 = Integer.parseInt(s2.getText());
                id2 = Integer.parseInt(s3.getText());
                id3 = Integer.parseInt(s4.getText());
                try {
                    o.insertOrder(new Order(id1, id2, id3));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            case 12://insert produs

                //id = Integer.parseInt(s1.getText());
                id1 = Integer.parseInt(s3.getText());
                id2 = Integer.parseInt(s4.getText());
                try {
                    p.insertProduct(new Product(s2.getText(), id1, id2));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            case 20://update client
                id = Integer.parseInt(s1.getText());
                i = Integer.parseInt(s4.getText());
                c.updateCostumer(new Costumer(id, s2.getText(), s3.getText(), i), id);
                break;
            case 21://update order
                id = Integer.parseInt(s1.getText());
                id1 = Integer.parseInt(s2.getText());
                id2 = Integer.parseInt(s3.getText());
                id3 = Integer.parseInt(s4.getText());
                o.updateOrder(new Order(id, id1, id2, id3), id);
                break;
            case 22://update produs
                id = Integer.parseInt(s1.getText());
                id1 = Integer.parseInt(s3.getText());
                id2 = Integer.parseInt(s4.getText());
                p.updateProduct(new Product(id, s2.getText(), id1, id2), id);
                break;
            case 23://delete client
                id = Integer.parseInt(s1.getText());
                c.deleteCostumer(id);
                break;
            case 24://delete order
                int iddd = Integer.parseInt(s1.getText());
                o.deleteOrder(iddd);
                break;
            case 25://delete produs
                id = Integer.parseInt(s1.getText());
                p.deleteProduct(id);
                break;
            case 14://print/////////////////////////////////////////////////////////////////////////////

                break;

        }

    }
}
