package presentation;

import java.awt.*;
import javax.swing.*;

class Edit extends JFrame {

    JPanel panel1, panel2, panel3, panel4;
    JLabel l1, l2, l3, l4;
    JTextField tf1, tf2, tf3, tf4;
    JButton b1, b2, b3, b4, b5, b6, b7, b8, b9;

    public Edit(String nume) {
        setTitle(nume);
        setSize(480, 640);

        panel1 = new JPanel();
        panel2 = new JPanel();
        panel3 = new JPanel();
        panel4 = new JPanel();

        l1 = new JLabel("Info 1:");
        l2 = new JLabel("Info 2:");
        l3 = new JLabel("Info 3:");
        l4 = new JLabel("Info 4:");

        tf1 = new JTextField("-");
        tf2 = new JTextField("-");
        tf3 = new JTextField("-");
        tf4 = new JTextField("-");

        tf1.setColumns(35);
        tf2.setColumns(35);
        tf3.setColumns(35);
        tf4.setColumns(35);

        panel1.add(l1);
        panel1.add(tf1);
        panel1.add(l2);
        panel1.add(tf2);
        panel1.add(l3);
        panel1.add(tf3);
        panel1.add(l4);
        panel1.add(tf4);

        panel1.setLayout(new FlowLayout());
        panel2.setLayout(new FlowLayout());
        b1 = new JButton("Add Client");
        b2 = new JButton("Add Order");
        b3 = new JButton("Add Product");
        b4 = new JButton("Update Client");
        b5 = new JButton("Update Order");
        b6 = new JButton("Update Product");
        b7 = new JButton("Delete Client");
        b8 = new JButton("Delete Order");
        b9 = new JButton("Delete Product");


        panel2.add(b1);
        panel2.add(b2);
        panel2.add(b3);
        panel3.add(b4);
        panel3.add(b5);
        panel3.add(b6);
        panel4.add(b7);
        panel4.add(b8);
        panel4.add(b9);

        b1.addActionListener(new Butoane(10, tf1, tf2, tf3, tf4));
        b2.addActionListener(new Butoane(11, tf1, tf2, tf3, tf4));
        b3.addActionListener(new Butoane(12, tf1, tf2, tf3, tf4));
        b4.addActionListener(new Butoane(20, tf1, tf2, tf3, tf4));
        b5.addActionListener(new Butoane(21, tf1, tf2, tf3, tf4));
        b6.addActionListener(new Butoane(22, tf1, tf2, tf3, tf4));
        b7.addActionListener(new Butoane(23, tf1));
        b8.addActionListener(new Butoane(24, tf1));
        b9.addActionListener(new Butoane(25, tf1));
        //b4.addActionListener(new Butoane(13, tf1, tf2, tf3));

        JPanel p = new JPanel();
        p.add(panel1);
        p.add(panel2);
        p.add(panel3);
        p.add(panel4);
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

        setContentPane(p);
        setVisible(true);
    }
}