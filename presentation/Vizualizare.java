package presentation;

import javax.swing.*;
import java.util.List;
import dao.*;

class Vizualizare extends JFrame {


    JTable tabel;

    public <T> Vizualizare(String nume, List<Object> obj) throws IllegalAccessException {
        setTitle(nume);
        setSize(480, 320);
        TableDAO dao=new TableDAO();
        try {
            tabel=dao.retrieveProperties(obj);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        JPanel p = new JPanel();
        p.setLayout(null);
        tabel.setBounds(30,50,400,200);

       p.add(tabel);
        setContentPane(p);
        setVisible(true);
    }
}