package presentation;

import java.awt.*;
import javax.swing.*;
import java.util.List;

class Factura extends JFrame {


    JPanel panel1, panel2;
    JLabel l1;
    JButton b1;

    public Factura(String nume, String s)
    {
        setTitle(nume);
        setSize(480, 320);

        panel1 = new JPanel();
        panel2 = new JPanel();
        l1= new JLabel(s);

        b1 = new JButton("Print");
        panel1.add(l1);
        panel2.add(b1);
        b1.addActionListener(new Butoane(14));
        JPanel p = new JPanel();
        p.add(panel1);
        p.add(panel2);
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

        setContentPane(p);
        setVisible(true);
    }
}