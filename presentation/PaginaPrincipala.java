package presentation;

import java.awt.*;
import javax.swing.*;

class PaginaPrincipala extends JFrame {


    JPanel panel1, panel2, panel3;
    JLabel l1;
    JTextField tf1;
    JButton b1, b2;

    public PaginaPrincipala(String nume)
    {
        setTitle(nume);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(480, 320);

        panel1 = new JPanel();
        panel2 = new JPanel();
        panel3 = new JPanel();

        l1 = new JLabel("ID Client");

        tf1 = new JTextField("-");

        tf1.setColumns(10);


        panel1.setLayout(new FlowLayout());
        panel2.setLayout(new FlowLayout());
        panel3.setLayout(new FlowLayout());

        b1 = new JButton("Administrare");
        b2 = new JButton("Client");
        b1.setPreferredSize(new Dimension(200, 40));
        b2.setPreferredSize(new Dimension(200, 40));

        panel1.add(b1);
        panel2.add(b2);
        panel3.add(l1);
        panel3.add(tf1);

        b1.addActionListener(new Butoane(0));
        b2.addActionListener(new Butoane(1, tf1));

        JPanel p = new JPanel();
        p.add(panel1);
        p.add(panel2);
        p.add(panel3);
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

        setContentPane(p);
        setVisible(true);
    }

    public static void main(String args[]) {
        new PaginaPrincipala("Pagina principala");
    }

}