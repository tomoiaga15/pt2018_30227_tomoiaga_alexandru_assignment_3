package presentation;

import java.awt.*;
import javax.swing.*;

class Client extends JFrame {


    JPanel panel1, panel2, panel3;
    JButton b1, b2, b3;
    JLabel l1;
    static int id;

    public Client(String nume, int id)
    {
        setTitle(nume);
        setSize(480, 320);

        panel1 = new JPanel();
        panel2 = new JPanel();
        panel3 = new JPanel();

        l1 = new JLabel("Clientul cu id: "+id);

        panel1.setLayout(new FlowLayout());
        panel2.setLayout(new FlowLayout());

        b1 = new JButton("Produse");
        b2 = new JButton("Comanda");

        panel3.add(l1);
        panel1.add(b1);
        panel2.add(b2);

        b1.setPreferredSize(new Dimension(200, 40));
        b2.setPreferredSize(new Dimension(200, 40));


        b1.addActionListener(new Butoane(8));
        b2.addActionListener(new Butoane(9));

        JPanel p = new JPanel();
        p.add(panel3);
        p.add(panel1);
        p.add(panel2);

        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

        setContentPane(p);
        setVisible(true);
    }
}