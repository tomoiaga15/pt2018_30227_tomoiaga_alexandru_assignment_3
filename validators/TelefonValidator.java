package validators;

import model.*;

public class TelefonValidator implements Validator<Costumer> {
    private static final int MIN_TEL = 700000000;
    private static final int MAX_TEL = 799999999;

    public void validate(Costumer t) {

        if (t.getTelefon() < MIN_TEL|| t.getTelefon() > MAX_TEL) {
            throw new IllegalArgumentException("The phone nummber limit is not respected!");
        }

    }

}