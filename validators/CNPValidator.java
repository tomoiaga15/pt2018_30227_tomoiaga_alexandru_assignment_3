package validators;

import model.Costumer;


public class CNPValidator implements Validator<Costumer> {

    public void validate(Costumer t) {

        if (t.getCnp().length()!=13) {
            throw new IllegalArgumentException("The Costumer CNP limit is not respected!");
        }

    }

}