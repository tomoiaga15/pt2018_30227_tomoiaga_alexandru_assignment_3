package model;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private int id, idClient, idProdus, cantitate;
    public Order(int id, int idClient, int idProdus, int cantitate) {
        this.id=id;
        this.idClient=idClient;
        this.idProdus=idProdus;
        this.cantitate=cantitate;
    }
    public Order(int idClient, int idProdus, int cantitate) {
        this.idClient=idClient;
        this.idProdus=idProdus;
        this.cantitate=cantitate;
    }
    public int getIdProdus() {
        return idProdus;
    }
    public void setIdProdus(int idProdus) {
        this.idProdus = idProdus;
    }
    public int getIdClient() {
        return idClient;
    }
    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getCantitate() {
        return cantitate;
    }
    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }
    public String toString() {
        return "Order [id=" + id + ", idClient=" + idClient + ", idProdus=" + idProdus + ", cantitate=" + cantitate+"]";
    }
}
