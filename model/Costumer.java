package model;

import java.util.ArrayList;
import java.util.List;

public class Costumer {
    private int id, telefon;
    private String cnp, nume;

    public Costumer(int id, String nume, String cnp, int telefon) {
        this.setId(id);
        this.setNume(nume);
        this.setCnp(cnp);
        this.setTelefon(telefon);
    }

    public Costumer(String nume, String cnp, int telefon) {
        this.setNume(nume);
        this.setCnp(cnp);
        this.setTelefon(telefon);
    }
    public Costumer() {};

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public int getTelefon() {
        return telefon;
    }

    public void setTelefon(int telefon) {
        this.telefon = telefon;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }
    public String toString() {
        return "Costumer [id=" + id + ", nume=" + nume + ", cnp=" + cnp + ", telefon= +40" + telefon+"]";
    }

}
