package model;

import java.util.ArrayList;
import java.util.List;

public class Product {
    private int id, pret, cantitate;
    private String denumire;
    public Product(int id, String denumire, int pret, int cantitate) {
        this.setId(id);
        this.setDenumire(denumire);
        this.setPret(pret);
        this.setCantitate(cantitate);
    }
    public Product(String denumire, int pret, int cantitate) {
        this.setDenumire(denumire);
        this.setPret(pret);
        this.setCantitate(cantitate);
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getCantitate() {
        return cantitate;
    }
    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }
    public String getDenumire() {
        return denumire;
    }
    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }
    public int getPret() {
        return pret;
    }
    public void setPret(int pret) {
        this.pret = pret;
    }
    public String toString() {
        return "Product [id=" + id + ", denumire=" + denumire + ", pret=" + pret + ", cantitate=" + cantitate+"]";
    }

}
